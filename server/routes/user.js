var express = require('express');
var router = express.Router();
var fetch = require('isomorphic-fetch');
var priv_info = require('../../priv_info');
var common = require('../../common');
 

var database = require('mysql').createConnection({
    host: 'localhost',
    user: 'root',
    password: priv_info.db_pwd,
    database: 'cs_trade'
});

setInterval(fetchFloatedInventory, 1 * 8 * 1000);

function getPriceObject(price) {
	return { price : price }
}

function fetchPriceInfo(item) {
	return new Promise(function(resolve, reject) { // Обновление запроса
		database.query('SELECT price FROM prices WHERE name = ? LIMIT 1', [item.market_hash_name], function(err, results, fields){	
			if(err) { console.log(err); return; }
			
			// Поиск значения
			if(results.length > 0){ 
				resolve( getPriceObject(results[0].price) );
				return;
			}
			else {
				// БД
				fetch('http://xxx.ru/api/GetItemPrice/?time=1&id=' + encodeURI(item.market_hash_name))
					.then(function(res) { return res.json(); })
					.then(function(json) {
						json = getPriceObject(json.lowest_price);
						
						// Сохранение в БД
						database.query('INSERT INTO prices (name, price) VALUES (?, ?) ON DUPLICATE KEY UPDATE price = VALUES(price)', 
								[item.market_hash_name, json.price], function(err, results, fields){});
								
						resolve(json);
					})
					.catch(err => console.log(err));
			}
		});
	});
}
function setPriceInfo(item){ 
	return fetchPriceInfo(item).then((price) => Promise.resolve(Object.assign(item, { price_info: price }))); 
}

function fetchFloatedInventory(steam_id = common.bot_id) {
	// Получение значения
	fetch('http://api.xxxx.ru/IEconItems_730/GetPlayerItems/v0001/?key='+priv_info.steam_api_key+'&SteamID='+steam_id)
	.then(function(res) { return res.json(); })
	.then(function(data) { 
		if(data && data.result && data.result.status == 1){
			var arr = [];
			
			// переменная
			data.result.items.map((item) => {
				// Поиск значения
				for(var i in item.attributes) {
					var attr = item.attributes[i];
					if(attr.defindex == 8){
						arr.push([steam_id, item.id, attr.float_value]);
						return;
					}
				}
			});

			// Сохраняем
			if(arr.length > 0) {
				database.query('INSERT INTO float_values (steam_id, assetid, value) VALUES ? ON DUPLICATE KEY UPDATE value = VALUES(value)', 
						[arr], function(err, results, fields){ if(err) console.log(err); });
			}
		}
	}); 
}

function fetchFloatValue(steam_id, assetid) {
	return new Promise(function(resolve, reject) { // Обновление
		database.query('SELECT value FROM float_values WHERE steam_id = ? AND assetid = ? LIMIT 1', [steam_id, assetid], function(err, results, fields){	
			if(err) { console.log(err); return resolve(-1); }
			return resolve(results.length > 0 ? results[0].value : -1);
		});
	});
}

function setFloatValue(steam_id, item){ 
	return fetchFloatValue(steam_id, item.assetid).then((f_val) => Promise.resolve(f_val == -1 ? item : Object.assign(item, { float_value: f_val }))); 
}
 
var _ = require('lodash');

function getInventory(steam_id, add_float_val = false) {
	// получение списка вещей
	
	if(steam_id !== common.bot_id) add_float_val = false; 
	
	return new Promise(function(resolve, reject) {
		fetch('http://xxx.ru/inventory/' + steam_id + '/730/2?l=english&count=5000')
		.then(function(res) { return res.json(); })
		.then(function(json) { 
			if(!json.error){
				// выбор описания
				var lookup = {};
				for (var i = 0, len = json.descriptions.length; i < len; i++) {
					lookup[json.descriptions[i].classid] = json.descriptions[i];
				}
				delete json.descriptions;
				
				for (var i = 0, len = json.assets.length; i < len; i++) {
					_.merge(json.assets[i], lookup[json.assets[i].classid]);
				}
				
				Promise.all(json.assets.filter((item) => {return item.tradable && item.marketable})
					// Получение прайса
					.map(setPriceInfo)).then((items) => { 
						delete json.total_inventory_count; 
						
						if(add_float_val){
							Promise.all(items.map((it) => setFloatValue(steam_id, it))).then((items_floated) => {
								json.assets = items_floated;
								return resolve(json); 
							});
						}
						else {
							json.assets = items;
							return resolve(json); 
						}
					});
			}
			else return resolve(json); 
		});
	});
}

router.post('/inventory', function(req, res) {
	getInventory(req.query.user_id, true).then(function (json) { return res.json(json); });
});
  
function getTradeURL(steam_id) {
	return new Promise(function(resolve, reject) {
		database.query('SELECT trade_url FROM user WHERE steam_id = ? LIMIT 1', [steam_id], function(err, results, fields){	
			if(err) { console.log(err); return; }
			
			// Поиск информации
			if(results.length > 0) {
				return resolve(results[0]);
			}
			return resolve( { trade_url : "" } );
		});
	});
}

router.get('/getTradeURL', function(req, res) {
	if(!req.user.id) { return res.json( { trade_url : "" } ); }
	getTradeURL(req.user.id).then(function (trade_url) { return res.json(trade_url); });
});

router.post('/setTradeURL', function(req, res) {
	if(!req.user.id) { return; }
	
	database.query('INSERT INTO user (steam_id, trade_url) VALUES(?, ?) ON DUPLICATE KEY UPDATE trade_url = VALUES(trade_url)', 
			[req.user.id, req.body.trade_url], function(err, results, fields){	
		if(err) { console.log(err); return  res.json( { status : 1 } ); }
		return res.json( { status : 0 } );
	});
});
  
  
  
  
  
  
  
  
  
  
  
  
  

// Включение
if(0) {

function validateOffer(user, bot, callback) {
    if(!user.items.length || user.items.length <= 0) return callback("User has no items in offer.");
	
	user.value = 0; user.alias = 'user';
	bot.value = 0; bot.alias = 'bot';
	
	var accs = [user, bot];
	
	Promise.all(accs.map((acc) => {
		return getInventory(acc.id).then((inventory) => {
			if(!inventory || !inventory.assets || inventory.assets.length <= 0) return Promise.reject("Couldn't fetch the " + user.alias + "'s inventory.");
			
			var is_user = user.alias === 'user';
			let count = 0;
			acc.items.map((assetid) => {
				for(var k in inventory.assets) {
					var item = inventory.assets[k];
					if(item.assetid === assetid && common.isValid(is_user, item.price_info.price)) {
						acc.value += parseFloat(parseFloat(item.price_info.price * common.getPriceRate(user.displayName, item.market_hash_name, item.type, acc.alias)).toFixed(2));
						count += 1;
						break;
					}
				}
			});
			
			if(count !== acc.items.length) return Promise.reject("Some items were not found in the " + user.alias + "'s inventory!");
		});
	})).then(() => { 
		if(parseFloat(user.value.toFixed(2)) < parseFloat(bot.value.toFixed(2))) return callback('You do not have enough value!');
		
		// Успех
		return callback(null, true);
	}).catch((err) => { return callback(err); });
};
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
// Стим 
var steamguard_file = "node_modules/priv_info/steamguard.txt";
var polldata_file = "node_modules/priv_info/polldata.json";

var SteamCommunity = require('steamcommunity');
var TradeOfferManager = require('steam-tradeoffer-manager');
var fs = require('fs');

var steam = new SteamCommunity();
var manager = new TradeOfferManager({
	"domain": "example.com", // Домен
	"language": "en", // Язык комментариев
	"pollInterval": 5000 // задержка обновления
});

// Авторизация стим
var logOnOptions = priv_info.bot_logOnOptions;

if (fs.existsSync(steamguard_file)) { logOnOptions.steamguard = fs.readFileSync(steamguard_file).toString('utf8'); }
if (fs.existsSync(polldata_file)) { manager.pollData = JSON.parse(fs.readFileSync(polldata_file)); }

steam.login(logOnOptions, function(err, sessionID, cookies, steamguard) {
	if (err) {
		console.log("Steam login fail: " + err.message);
		process.exit(1);
	}

	fs.writeFile(steamguard_file, steamguard);

	console.log("Logged into Steam");

	manager.setCookies(cookies, function(err) {
		if (err) {
			console.log(err);
			process.exit(1); // Ошибка ключа АПИ
			return;
		}

		console.log("Set cookies");
	});

	steam.startConfirmationChecker(30000, priv_info.identity_secret); // Checks and accepts confirmations every 30 seconds
});
// Стим БОТ

  
// БОТ
function makeInventory(asset_list) {
	return asset_list.map((assetid) => {
		return {
			assetid: assetid,
			appid: 730,
			contextid: 2,
			amount: 1
		};
	})
}

router.post('/sendOffer', function(req, res) {
	if(!req.user) return;
	
	var myItems = req.body.bot_items;
	var theirItems = req.body.user_items;
	
	if(!req.user.id) { return res.json( { status: 1, msg: "Bad auth." } ); }
	
	validateOffer({id: req.user.id, items: theirItems, displayName: req.user.displayName },
				  {id: common.bot_id, items: myItems, displayName: 'bot' }, (err, success) => {
		if(!err && success) {
			getTradeURL(req.user.id).then(function (obj) {
				var trade_url = obj.trade_url;
				if(trade_url === '') return res.json({status: 1, msg: 'Invalid trade URL.'});
				
				// Создание оффера
				var offer;
				try{ offer = manager.createOffer(trade_url); }
				catch(err){ return res.json({status: 1, msg: 'Invalid trade URL.'}) }
				
				offer.addMyItems(makeInventory(myItems));
				offer.addTheirItems(makeInventory(theirItems));
				
				offer.setMessage("Fair trade, sir! - " + common.site_tag + " Bot");
				
				offer.send(function(err, status) {
					if (err) {
						return res.json({status: 1, msg: err});
					}
					
					if (status == 'pending') {
						// Выбор
						steam.acceptConfirmationForObject(priv_info.identity_secret, offer.id, function(err) {
							if (err) {
								return res.json({status: 1, msg: "Bot couldn't do mobile confirmation."});
							} else {
								return res.json({status: 0, offer_id: offer.id});
							}
						});
					} else {
						//Логи
						return res.json({status: 0, offer_id: offer.id});
					}
				});
			}).catch(console.log);
		}
		else return res.json({status: 1, msg: err});
	});
});

manager.on('receivedOfferChanged', function(offer, oldState) {
	console.log(`Offer #${offer.id} changed: ${TradeOfferManager.ETradeOfferState[oldState]} -> ${TradeOfferManager.ETradeOfferState[offer.state]}`);

	if (offer.state == TradeOfferManager.ETradeOfferState.Accepted) {
		offer.getReceivedItems(function(err, items) {
			if (err) {
				console.log("Couldn't get received items: " + err);
			} else {
				var names = items.map(function(item) {
					return item.name;
				});

				console.log("Received: " + names.join(', '));
			}
		});
	}
});

manager.on('pollData', function(pollData) { fs.writeFile(polldata_file, JSON.stringify(pollData), function() {}); });

}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

module.exports = router;