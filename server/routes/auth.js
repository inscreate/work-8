var express = require('express')
  , router = express.Router()
  , passport = require('passport');

// GET /auth/steam
// подключаемся к стим АПИ
router.get('/steam',
  passport.authenticate('steam', { failureRedirect: '/' }),
  function(req, res) {
    res.redirect('/');
  });

// GET /auth/steam/return
//  Возврат запроса 
router.get('/steam/return',
  // Метод SHOP
  function(req, res, next) {
      req.url = req.originalUrl;
      next();
  }, 
  passport.authenticate('steam', { failureRedirect: '/' }),
  function(req, res) {
    res.redirect('/');
  });

module.exports = router;